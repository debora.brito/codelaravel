<?php

use App\Http\Controllers\adminController;
use App\Http\Controllers\AlunoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\UserController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\CursosController;
use App\Http\Controllers\InscricaoController;
use App\Http\Controllers\PostsController;
use App\Http\Controllers\RolesController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('/login', [UserController::class, 'login']);
Route::post('/register', [UserController::class, 'register']);
Route::post('/alunos', [AlunoController::class, 'store']);
Route::post('/admin', [adminController::class, 'store']);
Route::group(['middleware' => 'auth:api'], function () {
    Route::post('/details', [UserController::class, 'details']);
    Route::get('/users', [UserController::class, 'index']);
    Route::get('/posts', [PostsController::class, 'index']);
    Route::post('/posts', [PostsController::class, 'store']);
    Route::get('/posts/{id}', [PostsController::class, 'show']);
    Route::put('/posts/{id}', [PostsController::class, 'update']);
    Route::delete('/posts/{id}', [PostsController::class, 'destroy']);
    Route::get('/comentarios', [CommentsController::class, 'index']);
    Route::post('/comentarios', [CommentsController::class, 'store']);
    Route::put('/comentarios/{id}', [CommentsController::class, 'update']);
    Route::delete('/comentarios/{id}', [CommentsController::class, 'destroy']);
    Route::get('/alunos', [AlunoController::class, 'index']);
    Route::get('/alunos/{id}', [AlunoController::class, 'show']);
    Route::put('/alunos/{id}', [AlunoController::class, 'update']);
    Route::delete('/alunos/{id}', [AlunoController::class, 'destroy']);
    Route::get('/admin', [adminController::class, 'index']);
    Route::put('/admin/{id}', [adminController::class, 'update']);
    Route::delete('/admin/{id}', [adminController::class, 'destroy']);
    Route::get('/cursos', [CursosController::class, 'index']);
    Route::post('/cursos', [CursosController::class, 'store']);
    Route::get('/cursos/{id}', [CursosController::class, 'show']);
    Route::put('/cursos/{id}', [CursosController::class, 'update']);
    Route::get('/cursos/{curso}/inscricao', [InscricaoController::class, 'inscricao']);
    Route::get('/cursos/{curso}/desinscricao', [InscricaoController::class, 'desinscricao']);
    Route::get('/cursos/{curso}/completo', [InscricaoController::class, 'completo']);
    Route::delete('/cursos/{id}', [CursosController::class, 'destroy']);
    Route::get('/inscricao/cursos', [InscricaoController::class, 'index']);
    Route::get('/curso/{curso}', [InscricaoController::class, 'show']);
    Route::get('/roles', [RolesController::class, 'index']);
    Route::get('/roles/get-role', [RolesController::class, 'show']);
    Route::get('/dashboard', [InscricaoController::class, 'dashboard']);
});
