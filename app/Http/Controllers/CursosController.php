<?php

namespace App\Http\Controllers;

use App\Models\Curso;
use App\Models\Inscricao;
use App\Models\User;
use App\Models\UserCurso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class CursosController extends Controller
{

    function __construct()
    {
        $this->middleware('roles:Admin', ['except' => ['index', 'show']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cursos = Curso::all();
        return response(["cursos" => $cursos], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->hasFile('thumbnail')) {
            // Get filename with the extension
            $filenameWithExt = $request->file('thumbnail')->getClientOriginalName();
            // Get just filename
            $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
            // Get just ext
            $extension = $request->file('thumbnail')->getClientOriginalExtension();
            // Filename to store
            $fileNameToStore = $filename . '_' . time() . '.' . $extension;
            // Upload Image
            $path = $request->file('thumbnail')->storeAs('public/thumbnail', $fileNameToStore);
        } else {
            $fileNameToStore = 'noimage.png';
        }
        $curso = new Curso();
        $curso->nome_curso = $request->nome_curso;
        $curso->descricao = $request->descricao;
        $curso->thumbnail =  $fileNameToStore;
        $curso->video = $request->video;
        $curso->user_id = Auth::id();
        $curso->save();
        $author = User::find($curso->user_id);
        return response(["curso" => $curso, "author" => $author], 201);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        $curso = Curso::find($id);

        if (is_null($curso)) {
            // return response
            $response = [
                'success' => false,
                'message' => 'curso not found.',
            ];
            return response()->json($response, 404);
        }

        // return response
        $response = [
            'success' => true,
            'message' => 'curso encontrado com sucesso.',
        ];
        return response()->json(["sucesso" => $response, "curso" => $curso], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $curso = Curso::findOrFail($id)->update($request->all());
        if (is_null($curso)) {
            // return response
            $response = [
                'success' => false,
                'message' => 'curso not found.',
            ];
            return response()->json($response, 404);
        }

        $response = [
            'success' => true,
            'message' => 'curso editado com sucess.',
        ];
        return response()->json([$response, "curso" => $curso], 200);
    }




    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Curso::findOrFail($id)->delete();
        $response = [
            'success' => true,
            'message' => 'curso deletado com sucess',
        ];
        return response()->json($response, 200);
    }
}
