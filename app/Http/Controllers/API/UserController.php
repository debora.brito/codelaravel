<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use App\Models\User; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Support\Facades\Validator;

class UserController extends Controller 
{

    protected $user;
    public $successStatus = 200;
/** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(){ 
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){ 
            $id    = auth()->user()->id;
            $user  = User::find($id);
            $token = $user->createToken('auth')->accessToken;
            return response(['message' => 'success', 'access_token' => $token, "user" => $user], 201);
    
        } 
        else{ 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }
/** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    // public function register(Request $request) 
    // { 
    //     $validator = Validator::make($request->all(), [ 
    //         'name' => 'required', 
    //         'email' => 'required|email', 
    //         'password' => 'required', 
    //         'serie' => 'required',
    //     ]);
    //     if ($validator->fails()) { 
    //         return response()->json(['error'=>$validator->errors()], 401);            
    //     }
    //     $input = $request->all(); 
    //     $input['password'] = bcrypt($input['password']); 
    //     $user = User::create($input); 
    //     $success['token'] =  $user->createToken('MyApp')-> accessToken; 
    //     $success['name'] =  $user->name;
    //     return response()->json(['success'=>$success], $this-> successStatus); 
    // }
/** 
     * details api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function details() 
    { 
        $user = Auth::user(); 
        return response()->json(['user' => $user], $this-> successStatus); 
    }
    
    public function index(){
        $users = User::all();
        return response()->json(['users' => $users]);
    }

    public function logout(Request $request)
    {
        $user = $request->user();

        if ($user->token()->revoke()) {
            return response(['message' => 'Successfully logged out.']);
        }
    }

}
