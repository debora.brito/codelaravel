<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;


class PostsController extends Controller
{
    function __construct()
    {
        $this->middleware('roles:Estudante,Admin,Professor');
    }

    public function index()
    {
        $posts = Post::all();
        return response(["posts" => $posts], 200);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $post = new Post();
        $post->titulo = $request->titulo;
        $post->descricao = $request->descricao;
        $post->conteudo = $request->conteudo;
        $post->user_id = auth()->user()->id;
        $post->save();
        return response(["post" => $post], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::find($id);

        if (is_null($post)) {
            // return response
            $response = [
                'success' => false,
                'message' => 'post not found.',
            ];
            return response()->json($response, 404);
        }

        // return response
        $response = [
            'success' => true,
            'message' => 'post retrieved successfully.',
        ];
        return response()->json(["sucesso" => $response, "post" => $post], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Post::findOrFail($id)->update($request->all());
        if (is_null($post)) {
            // return response
            $response = [
                'success' => false,
                'message' => 'post not found.',
            ];
            return response()->json($response, 404);
        }

        // return response
        $response = [
            'success' => true,
            'message' => 'post editado com sucess.',
        ];
        return response()->json([$response, "post" => $post], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id)->delete();
        $response = [
            'success' => true,
            'message' => 'Post deletado com sucess',
        ];
        return response()->json($response, 200);
    }
}
