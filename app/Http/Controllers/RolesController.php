<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Role;
use App\Models\User;
use App\Models\UserRole;
use Illuminate\Support\Facades\Auth;

class RolesController extends Controller
{
    function __construct()
    {
        $this->middleware('roles:Estudante,Admin,Professor');
    }

    public function index(){
        $roles = Role::all();
        return response()->json(["roles" => $roles], 200);
    }

    public function show(){
        $userRole = UserRole::where('user_id', '=', Auth::id())->get()->first();
        return response()->json(["Role" => $userRole], 200);
    }

}
