<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\User;
use Illuminate\Http\Request;

class adminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = Admin::all();
        return response($admins);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $admin = new Admin();

        $admin->name = $request->name;
        $admin->email = $request->email;


        $user = new User();
        $user->name = $admin->name;
        $user->email = $admin->email;
        $user->password = bcrypt($request->password);
        $user->save();
        // dd($user->role());
        $user->role()->attach(1);
        // Atribui ao admin o id do usuário que foi criado
        $admin->user_id = $user->id;
        $admin->save();
        return response()->json(["Admin" => $admin]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $admin = Admin::find($id);

        if (is_null($admin)) {
            // return response
            $response = [
                'success' => false,
                'message' => 'admin not found.',
            ];
            return response()->json($response, 404);
        }

        // return response
        $response = [
            'success' => true,
            'message' => 'admin retrieved successfully.',
        ];
        return response()->json(["sucesso" => $response, "admin" => $admin], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = admin::findOrFail($id);

        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->password = bcrypt($request->password);

        $user = User::find($id);
        $input = $request->all();
        $user->name = $admin->name;
        $user->email = $admin->email;
        $user->password = bcrypt($admin->password);
        $user->save();
        $user->role()->sync($input['role']);
        $admin->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_admin = Admin::find($id);
        $admin = Admin::findOrFail($id)->delete();
        User::find($user_admin->user_id)->delete();
        return response(['sucess' => 'admin excluido com sucesso'], 201);    
    }
}
