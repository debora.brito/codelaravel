<?php

namespace App\Http\Controllers;

use App\Models\Curso;
use App\Models\Inscricao;
use App\Models\User;
use App\Models\UserCurso;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class InscricaoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            // dd(UserCurso::all());
            $userCurso = UserCurso::where('user_id', '=', Auth::id())->get();

            $cursos = [];
            foreach ($userCurso as $row) {
                $cursos[] = $row->curso;
            }
            $cursos = collect($cursos);
        } else {
            $cursos = Curso::all();
        }
        if ($cursos->isEmpty()) {
            return response('Não está inscrito em nenhum curso');
        } else {
            foreach ($cursos as $curso) {
                $cursos->author = User::find($curso->user_id);
            }
        }
        return response(["cursos" => $cursos], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Curso $curso)
    {
        if (Auth::check()) {
            $user = Auth::user();
            $text = (UserCurso::where('user_id', '=', $user->id)->where('curso_id', '=', $curso->id)->get()->first());
            $enroll = (isset($text)) ? $text->curso_andamento : '';
            $comp = UserCurso::where('user_id', '=', $user->id)->where('curso_id', '=', $curso->id)->get()->first();
            $complete = (isset($comp) && $comp->curso_completo == 1) ? $comp->curso_completo : false;
        } else {
            $enroll = false;
            $complete = false;
        }
        $author = User::find($curso->user_id);
        // dd($author);
        return response(["curso" => $curso, "autor" => $author, "inscricao" => $enroll, "completo" => $complete], 201);
    }

    public function inscricao(User $a, Curso $curso)
    {
        $user = Auth::id();
        if (Auth::guest()) {
            return response()->json("Faça Login");
        }
        $inscricao = new Inscricao();
        $inscricao->user_id = $user;
        $inscricao->curso_id = $curso->id;
        $inscricao->status = 0;
        $inscricao->save();

        $userCurso = new UserCurso();
        $userCurso->user_id = $user;
        $userCurso->curso_id = $curso->id;
        $userCurso->curso_andamento = 1;
        $userCurso->curso_completo = 0;
        $userCurso->save();
        return response()->json(['Inscricao' => $inscricao, "userCurso" => $userCurso, "message" => 'Inscrito com sucesso!']);
    }

    public function desinscricao(Curso $curso)
    {
        //detach record from user-course.
        UserCurso::where('user_id', '=', Auth::id())
            ->where('curso_id', '=', $curso->id)
            ->delete();
        Inscricao::where('user_id', '=', Auth::id())
            ->where('curso_id', '=', $curso->id)
            ->delete();
        return response()->json('Você foi desinscrito do curso');
    }

    public function completo(Curso $curso)
    {
        UserCurso::where('user_id', '=', Auth::id())
            ->where('curso_id', '=', $curso->id)
            ->update(['curso_completo' => 1]);
        return response()->json(["completo" => $curso->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function dashboard()
    // {
    //     $user = Auth::user();
    //     $cursos = Curso::where('user_id', '=', $user->id);
    //     $cursos = $cursos->pluck('id')->all();
    //     $inscricao = Inscricao::whereIn('curso_id', $cursos)->get();
    //     return response()->json(['cursos' => $cursos, "inscricao" => $inscricao]);
    // }
}
