<?php

namespace App\Http\Controllers;

use App\Models\Aluno;
use App\Models\User;
use Illuminate\Http\Request;

class AlunoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $alunos = Aluno::all();
        return response($alunos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $aluno = new Aluno();

        $aluno->name = $request->name;
        $aluno->email = $request->email;
        // $aluno->password = bcrypt($request->password);
        $aluno->serie = $request->serie;

        $user = new User();
        $input = $request->all();
        $user->name = $aluno->name;
        $user->email = $aluno->email;
        $user->password = bcrypt($request->password);
        $user->save();
        $user->role()->attach(3);
        // Atribui ao aluno o id do usuário que foi criado
        $aluno->user_id = $user->id;
        $aluno->save();
        return response()->json(['user' => $user]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $aluno = Aluno::find($id);

        if (is_null($aluno)) {
            // return response
            $response = [
                'success' => false,
                'message' => 'aluno not found.',
            ];
            return response()->json($response, 404);
        }

        // return response
        $response = [
            'success' => true,
            'message' => 'aluno retrieved successfully.',
        ];
        return response()->json(["sucesso" => $response, "aluno" => $aluno], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $aluno = Aluno::findOrFail($id);

        $aluno->name = $request->name;
        $aluno->email = $request->email;
        $aluno->serie = $request->serie;

        $user = User::find($id);
        $user->name = $aluno->name;
        $user->email = $aluno->email;
        $user->password = bcrypt($request->password);
        $user->serie = $aluno->serie;
        $user->save();
        $aluno->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user_aluno = Aluno::find($id);
        $aluno = Aluno::findOrFail($id)->delete();
        User::find($user_aluno->user_id)->delete();
        return response(['sucess' => 'Aluno excluido com sucesso'], 201);
    }
}
