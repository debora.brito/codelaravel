<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\User;
use App\Models\Post;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $comentarios = Comment::all();
        return response(["comentarios" => $comentarios], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $comentarios = new Comment();
        $post = Post::find($request->post_id);
        $comentarios->resposta = $request->resposta;
        $comentarios->user_id = auth()->user()->id;
        $comentarios->post_id = $request->post_id;
        $comentarios->save();
        return response(["comentarios" => $comentarios, "post" => $post], 201);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $comentario = Comment::findOrFail($id)->update($request->all());
        if (is_null($comentario)) {
            // return response
            $response = [
                'success' => false,
                'message' => 'comentario not found.',
            ];
            return response()->json($response, 404);
        }

        // return response
        $response = [
            'success' => true,
            'message' => 'comentario editado com sucess.',
        ];
        return response()->json([$response, "comentario" => $comentario], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $comentarios = Comment::findOrFail($id)->delete();
        $response = [
            'success' => true,
            'message' => 'Post deletado com sucess',
        ];
        return response()->json($response, 200);
    }
}
