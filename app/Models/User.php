<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function post()
    {
        return $this->hasMany(Post::class);
    }

    public function aluno()
    {
        return $this->hasOne(Aluno::class, 'user_id');
    }

    public function admin()
    {
        return $this->hasOne(Admin::class, 'user_id');
    }

    public function role()
    {
        return $this->belongsToMany(Role::class, 'user_roles', 'user_id');
    }

    public function cursos()
    {
        return $this->belongsToMany(Curso::class, 'user_curso', 'user_id');
    }

    public function setAttribute($key, $value)
    {
        $isRememberTokenAttribute = $key == $this->getRememberTokenName();
        if (!$isRememberTokenAttribute) {
            parent::setAttribute($key, $value);
        }
    }

    public function andamento()
    {
        return $this->hasMany(Andamento::class);
    }

    public function userCurso()
    {
        return $this->hasMany(UserCurso::class);
    }

    // public function hasRoles(array $roles){
    //     foreach($roles as $role){
    //         if($this->role === $role){
    //             return true;
    //         }
    //     }
    //     return false;
    // }
}
