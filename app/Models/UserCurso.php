<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserCurso extends Model
{
    use HasFactory;

    protected $fillable = ['user_id', 'curso_id', 'curso_andamento', 'curso_completo'];
    protected $primaryKey = 'curso_id';

    protected $table = "user_cursos";

    public function User()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function curso()
    {
        return $this->belongsTo(Curso::class, 'curso_id');
    }

}
