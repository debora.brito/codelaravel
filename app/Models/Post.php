<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $table = 'posts';
    protected $fillable = ['titulo', 'descricao', 'conteudo'];

    public function usuario()
    {
        return $this->belongsTo(User::class);
    }

    public function comments(){
        return $this->hasMany(Comments::class, 'post_id');
    }
}
