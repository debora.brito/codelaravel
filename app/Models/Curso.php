<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Curso extends Model
{
    use HasFactory;

    protected $fillable = [
        'nome_curso', 'descricao', 'video', 'user_id', 'progresso', 'thumbnail'
    ];

    protected $table ='cursos';

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function andamento()
    {
        return $this->hasMany(Inscricao::class);
    }

    public function userCurso()
    {
        return $this->hasMany(UserCurso::class);
    }
}
